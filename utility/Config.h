//
// Created by xf-huang on 2021/5/21.
//

#ifndef IRONDET_CONFIG_H
#define IRONDET_CONFIG_H


#include <QString>
#include <QSettings>
#include <memory>

class Config {
public:
    static Config &get() {
        static Config config;
        return config;
    }

    Config(const Config &) = delete;

    void operator=(const Config &) = delete;

    int getBufferLength() const;

    int getCamCount() const;

    QString getSavePath() const;

    QString getHost() const;

    int getPort() const;

    QString getUsername() const;

    QString getPassword() const;

    QString getDbName() const;

private:
    Config();

    bool valid;
    QString p;
    std::unique_ptr<QSettings> settings;
};



#endif //IRONDET_CONFIG_H
