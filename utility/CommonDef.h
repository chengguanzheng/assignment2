//
// Created by xf-huang on 2020/12/23.
//

#pragma once

#include "IdBase.h"

// Image related constants
#define IMG_WIDTH 1920
#define IMG_HEIGHT 1080
#define IMG_CHANNEL 3
#define IMAGE_KEY_TEMPLATE "image_{}"

template <int w, int h, int ch>
struct ImageData : IdBase {
    long long timestamp {};
    int width = w;
    int height = h;
    int channel = ch;
    char data[w * h * ch] {};
};

// used for images with fixed size
using ImageData1080p = ImageData<1920, 1080, 3>;

// used for images with variant size
struct ImageHeader : IdBase
{
    long long timestamp = 0;
    int width = 0;
    int height = 0;
    int channel = 0;
};

// You can also define custom structs, for example
struct FrameHeader : IdBase
{
    long long timestamp = 0;
    int width = 0;
    int height = 0;
    int channel = 0;
    int fps = 30;
};
