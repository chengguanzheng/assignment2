//
// Created by xf-huang on 2020/10/24.
//

#pragma once

#include <cassert>
#include <vector>

#include "RingCache.h"

template <class T>
class RingBuffer {
public:
    explicit RingBuffer(int cap = 10);
    ~RingBuffer() = default;
    bool empty() const;
    bool push(T& obj);
    T pop();
    int size() const;

private:
    RingCache cache;
    int typeSize;
};

template <class T>
RingBuffer<T>::RingBuffer(int cap)
    : cache(RingCache(cap, sizeof(T)))
    , typeSize(sizeof(T)) {}

template <class T>
bool RingBuffer<T>::push(T& obj) {
    cache.push(&obj, typeSize);
    return true;
}

template <class T>
T RingBuffer<T>::pop() {
    T tmp;
    cache.pop(&tmp, typeSize);
    return tmp;
}

template <class T>
bool RingBuffer<T>::empty() const {
    return cache.empty();
}

template <class T>
int RingBuffer<T>::size() const {
    return cache.size();
}
