//
// Created by xf-huang on 12/22/20.
//

#pragma once

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "RingCache.h"

class Repository {
public:
    Repository() = default;;

    ~Repository() = default;

    Repository(const Repository& repo) = delete;

    bool allocate(const std::string& key, int length, int eleSize, bool withId = false);

    bool free(const std::string& key);

    bool getById(const std::string& key, long long int id, void* pVoid, int maxSize) const;

    bool push(const std::string& key, void* pVoid, int size);

    bool hasKey(const std::string& key) const;

private:
    std::unordered_set<std::string> mKeySet;
    std::unordered_set<std::string> mKeyWithId;
    std::unordered_map<std::string, RingCache> mVecMap;
};
