//
// Created by Lenovo on 2021/6/30.
//

#include "DetectHandler.h"
#include <utility>
#include "opencv2/opencv.hpp"
#include "spdlog/spdlog.h"

using namespace std;
using namespace cv;

#define MODNAME "DetectHandler"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

DetectHandler::DetectHandler(Dispatcher &dispatcher, std::shared_ptr<Repository> repository,
                             std::string searchKey, std::string saveKey, HandlerType handlerType,
                             SignalType signalType,
                             std::vector<SignalType> listenSignalTypes):
        SignalHandler(dispatcher, handlerType, signalType,listenSignalTypes),
        repository(std::move(repository)), searchKey(searchKey), saveKey(saveKey)
{}

void DetectHandler::doJobsLoop() {
    while(!mAbQuitDoJobsLoop){
        {
            std::unique_lock<std::mutex> lck(mMtxJobsBuffer);
            if(mJobsBuffer.empty()){
                mCvJobsBuffer.wait(lck);  //在signalHandler中的assignJobs函数中唤醒
            }
            if(!mJobsBuffer.empty()) job = mJobsBuffer.pop();
        }
        if(mAbQuitDoJobsLoop) break;


        if (!(repository->hasKey(saveKey))) { //如果不包含saveKey类型的RIngCache，则创建
            repository->allocate(saveKey, 10, job.dataSize, true);
        }


        imageBuffer.resize(job.dataSize);
        bool found = repository->getById(searchKey, job.imageId, reinterpret_cast<void*>(imageBuffer.data()),
                                         job.dataSize);

        if (!found) {
            INFO(format("Image id {}  not found", job.imageId));
            continue;
        }

        auto header = reinterpret_cast<FrameHeader*>(imageBuffer.data());
        header->timestamp = chrono::system_clock::now().time_since_epoch().count();

        auto img = Mat(header->height, header->width, CV_8UC(header->channel), imageBuffer.data() + sizeof(FrameHeader));
        GaussianBlur(img, img, Size(15, 15), 11, 11);//高斯滤波

        repository->push(saveKey, imageBuffer.data(), imageBuffer.size());

        sendReadySignal(imageBuffer.size());
        DEBUG(format("Processed image {}", job.imageId));
        //INFO(format("get here"));
    }
}


long long DetectHandler::getImageId(){
    return (reinterpret_cast<FrameHeader*>(imageBuffer.data()))->id;
}
















