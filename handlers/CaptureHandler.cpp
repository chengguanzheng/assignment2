#include "CaptureHandler.h"

#include <utility>
#include "opencv2/opencv.hpp"
#include "spdlog/spdlog.h"

using namespace std;
using namespace cv;

#define MODNAME "CaptureHandler"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

CaptureHandler::CaptureHandler(
        Dispatcher &dispatcher, std::shared_ptr <Repository> repository,
        std::string key, HandlerType handlerType, SignalType sendSignalType,
        std::vector <SignalType> listenSignalTypes) :
        SignalHandler(dispatcher, handlerType, sendSignalType, std::move(listenSignalTypes)), idx(0),
        repository(std::move(repository)), repoKey(std::move(key)), quitCaptureLoop(true) {
}

void CaptureHandler::captureLoop() {
    if (filepath.empty()) {
        LOG_ERROR("Filepath is empty!");
        return;
    }
    VideoCapture cap(filepath);
    if (!cap.isOpened()) {
        LOG_ERROR(fmt::format("Failed to open file {}", filepath));
        return;
    }

    INFO(fmt::format("Reading from {}...", filepath));

    // Extract meta data from video file
    auto fps = cap.get(cv::CAP_PROP_FPS);
    auto channel = 3;
    auto width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
    auto height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

    int dataSize = sizeof(FrameHeader) + width * height * channel;
    buffer.resize(dataSize);
    auto header = (FrameHeader *) buffer.data();
    header->fps = fps;
    header->channel = channel;
    header->width = width;
    header->height = height;

    INFO(fmt::format("FPS: {}, frame size {}x{}x{}", header->fps, header->width, header->height, header->channel));

    int milliSecPerFrame = 1000 / header->fps;

    // init repository
    // Repository is dynamically managed here because the size of element is not fixed.
    // If the size of elements can be pre-determined, allocation can be elevated to the upper level class,
    // see https://gitlab.com/seu228/detect-SH/-/blob/dev/System.cpp#L20 for example
    if (repository->hasKey(repoKey)) repository->free(repoKey);
    repository->allocate(repoKey, 10, dataSize, true);

    // read images frame by frame
    Mat frame(height, width, CV_8UC((int) channel), buffer.data() + sizeof(FrameHeader));
    while (!quitCaptureLoop) {
        while (!quitCaptureLoop && cap.read(frame)) {
            header->id = idx++;
            header->timestamp = chrono::system_clock::now().time_since_epoch().count();
            repository->push(repoKey, buffer.data(), buffer.size());
            sendReadySignal(dataSize);

            DEBUG(fmt::format("Processed image {}.", header->id));

            this_thread::sleep_for(chrono::milliseconds(milliSecPerFrame));
        }
        cap.set(cv::CAP_PROP_POS_FRAMES, 0);
        DEBUG("Go back to the beginning of video file...");
    }
    INFO(fmt::format("Quit reading images from {}", filepath));
}

// This function is left empty because this is a `capture handler` (where all things start)
void CaptureHandler::doJobsLoop() {
}

long long CaptureHandler::getImageId() {
    return ((FrameHeader *) buffer.data())->id;
}

bool CaptureHandler::startCapture(const string &filepath) {

    // Stop last capture loop if there is one
    stopCapture();

    this->filepath = filepath;
    quitCaptureLoop = false;
    captureThread = thread(&CaptureHandler::captureLoop, this);

    if (!captureThread.joinable()) {
        LOG_ERROR(fmt::format("Failed to start capture loop with file {}", this->filepath));
        return false;
    }
    INFO("Capture loop started.");
    return true;
}

bool CaptureHandler::stopCapture() {
    if (captureThread.joinable()) {
        INFO(fmt::format("Stopping capture thread for {}", this->filepath));
        quitCaptureLoop = true;
        captureThread.join();
    }
    return true;
}

CaptureHandler::~CaptureHandler() {
    stopCapture();
}
