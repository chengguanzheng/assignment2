//
// Created by allenjuly7 on 2021/6/27.
//

#include "DisplayHandler.h"
#include "spdlog/spdlog.h"
#include <utility>

#define MODNAME "DisplayHandler"
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)

DisplayHandler::DisplayHandler(
        Dispatcher &dispatcher, std::string key, HandlerType handlerType, SignalType sendSignalType,
        std::vector<SignalType> listenSignalTypes) :
        SignalHandler(dispatcher, handlerType, sendSignalType, std::move(listenSignalTypes)), repoKey(std::move(key)) {

}

long long DisplayHandler::getImageId() {
    // not necessary if the handler does not send signals
    return 0;
}

void DisplayHandler::doJobsLoop() {
    while (!mAbQuitDoJobsLoop) {
        {
            std::unique_lock<std::mutex> lck(mMtxJobsBuffer);
            if (mJobsBuffer.empty()) {
                mCvJobsBuffer.wait(lck);
            }
            if (!mJobsBuffer.empty()) job = mJobsBuffer.pop();
            else continue;
        }

        emit  imageReady(repoKey, job.imageId, job.dataSize);
        //INFO(fmt::format("get here"));
    }
}
