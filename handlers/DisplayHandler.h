//
// Created by allenjuly7 on 2021/6/27.
//

#ifndef FRAMEWORK_STARTER_DISPLAYHANDLER_H
#define FRAMEWORK_STARTER_DISPLAYHANDLER_H

#include "QObject"
#include "framework/SignalHandler.h"

class DisplayHandler : public QObject, public SignalHandler {
Q_OBJECT
public:

    explicit DisplayHandler(
            Dispatcher &dispatcher,
            std::string key,
            HandlerType handlerType = HandlerType::DISPLAY,
            SignalType sendSignalType = SignalType::DISPLAY_READY,
            std::vector<SignalType> listenSignalTypes = {SignalType::DETECT_READY});

    ~DisplayHandler() override = default;

signals:

    void imageReady(const std::string& key, long long imageId, int dataSize);

private:

    long long getImageId() override;

    void doJobsLoop() override;

    std::string repoKey;

    Job job{};

};


#endif //FRAMEWORK_STARTER_DISPLAYHANDLER_H
