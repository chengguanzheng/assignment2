//
// Created by Lenovo on 2021/6/30.
//

#pragma once

#include "framework/SignalHandler.h"
#include "utility/Repository.h"
#include "utility/CommonDef.h"

class DetectHandler: public SignalHandler
{
public:
    explicit DetectHandler(Dispatcher& dispatcher,
                           std::shared_ptr<Repository> repository, std::string searchKey, std::string saveKey,
                           HandlerType handlerType = HandlerType::DETECT,
                           SignalType signalType = SignalType::DETECT_READY,
                           std::vector<SignalType> listenSignalTypes = {SignalType::IMAGE_READY});

    ~DetectHandler() override = default;
private:
    std::shared_ptr<Repository> repository;

    std::string searchKey;
    std::string saveKey;

    Job job{};

    std::vector<char> imageBuffer;

    void doJobsLoop() override;
    long long getImageId() override;
};