//
// Created by xf-huang on 2020/10/24.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <unordered_map>

#include "DispatchDef.h"
#include "utility/RingBuffer.h"

// solve cross #include issue
class SignalHandler;

class Dispatcher {
public:
    Dispatcher();

    ~Dispatcher();

    // register handlers with the dispatcher
    bool registerHandler(const std::shared_ptr<SignalHandler>& handler, bool on = true);

    // unregister handlers from the dispatcher
    bool unregisterHandler(const std::shared_ptr<SignalHandler>& handler);

    // start handler loops and signal processing loop
    void activateHandlers();

    // stop all handler loops and signal processing loop
    void deactivateHandlers();

    // will be called by the handlers
    void emitSignal(Signal& signal);

    void setWorkingMode(const std::unordered_map<HandlerType, bool>& map);

    const std::unordered_map<HandlerType, bool>& getWorkingMode() const;

private:
    // signal processing components
    RingBuffer<Signal> mRBSignalBuffer;
    std::condition_variable mCvSignalBuffer;
    std::mutex mMtxSignalBuffer;

    // Signal Processing Thread
    // process signals from signal buffer until its empty,
    // listen to condition variable then, wake when cv is notified.
    std::atomic_bool mAbQuitSignalProcess;
    std::thread mThSignalProcess;

    void signalProcessLoop();

    // hold registered handlers and signal-to-handler mapping
    int mNextHandlerId = 0;
    std::unordered_map<int, std::shared_ptr<SignalHandler>> mMapId2Handler;
    std::unordered_map<SignalType, std::vector<int>> mMapSignalType2HandlerIds;

    // hold working status of handlers (on or off)
    // Current implementation controls working status according to handler type
    // It can also be managed on the handler id level
    std::mutex mMtxHandlerStatus;
    std::unordered_map<HandlerType, bool> mMapHandlerStatus;

    int mDisplayHandlerId;
    int mNetworkHandlerId;
};
