//
// Created by xf-huang on 2020/10/24.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "utility/RingBuffer.h"
#include "framework/DispatchDef.h"
#include "framework/Dispatcher.h"

class SignalHandler {
public:
    explicit SignalHandler(
        Dispatcher& dispatcher,
        HandlerType handlerType = HandlerType::UNDEFINED,
        SignalType sendSignalType = SignalType::UNDEFINED,
        std::vector<SignalType> listenSignalTypes = { SignalType::UNDEFINED });

    virtual ~SignalHandler();

    void assignJobs(Job& job);

    int countPendingJobs() const;

    HandlerType getHandlerType() const;

    SignalType getSendSignalType() const;

    const std::vector<SignalType>& getListenSignalTypes() const;

    bool startDoJobsLoop();

    void stopDoJobsLoop();

private:
    std::thread mThDoJobs;

    // handler properties
    HandlerType mHandlerType;
    SignalType mSendSignalType;
    std::vector<SignalType> mVecListeningSignalTypes;

    // implement processing procedures for different modules
    virtual void doJobsLoop() = 0;

    virtual long long getImageId() = 0;

protected:
    // holds pending jobs
    RingBuffer<Job> mJobsBuffer;
    std::mutex mMtxJobsBuffer;
    std::atomic_bool mAbQuitDoJobsLoop;
    std::condition_variable mCvJobsBuffer;

    // reference from dispatcher, used to send signal to dispatcher
    Dispatcher& mRefDispatcher;

    void sendReadySignal(int dataSize);
};
