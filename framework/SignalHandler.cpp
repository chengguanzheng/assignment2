//
// Created by xf-huang on 2020/10/24.
//

#include "SignalHandler.h"
#include "spdlog/spdlog.h"

#define MODNAME "SignalHandler"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

SignalHandler::SignalHandler(
    Dispatcher& dispatcher,
    HandlerType handlerType,
    SignalType sendSignalType,
    std::vector<SignalType> listenSignalTypes)
    : mAbQuitDoJobsLoop(true)
    , mRefDispatcher(dispatcher) {

    mHandlerType = handlerType;
    mSendSignalType = sendSignalType;
    mVecListeningSignalTypes = std::move(listenSignalTypes);

    INFO(fmt::format("Connecting module {} with signal {}", explain(mHandlerType), explain(mSendSignalType)));
}

SignalHandler::~SignalHandler() {
    stopDoJobsLoop();
}

void SignalHandler::stopDoJobsLoop() {
    mAbQuitDoJobsLoop = true;
    mCvJobsBuffer.notify_all();
    if (mThDoJobs.joinable()) {
        mThDoJobs.join();
    }
    INFO(fmt::format("Stopped SignalHandler::doJobsLoop HandlerType = {}", explain(mHandlerType)));
}

bool SignalHandler::startDoJobsLoop() {
    // module type should be set before it is started
    if (mHandlerType == HandlerType::UNDEFINED) {
        ERROR("HandlerType must be set before starting doJobsLoop");
        return false;
    }

    mAbQuitDoJobsLoop = false;
    mThDoJobs = std::thread(&SignalHandler::doJobsLoop, this);
    INFO(fmt::format("Started SignalHandler::doJobsLoop HandlerType = {}", explain(mHandlerType)));
    return true;
}

int SignalHandler::countPendingJobs() const {
    return mJobsBuffer.size();
}

void SignalHandler::assignJobs(Job& job) {
    {
        std::lock_guard<std::mutex> lck(mMtxJobsBuffer);
        mJobsBuffer.push(job);
    }
    DEBUG(fmt::format("Got job from {} with image id = {}", explain(job.originator), job.imageId));
    mCvJobsBuffer.notify_all();
}

void SignalHandler::sendReadySignal(int dataSize) {
    assert(mSendSignalType != SignalType::UNDEFINED);
    Signal signal { mSendSignalType, getImageId(), mHandlerType, dataSize };
    mRefDispatcher.emitSignal(signal);

    DEBUG(fmt::format("{} send {}", explain(mHandlerType), explain(mSendSignalType)));
}

HandlerType SignalHandler::getHandlerType() const {
    return mHandlerType;
}

SignalType SignalHandler::getSendSignalType() const {
    return mSendSignalType;
}

const std::vector<SignalType>& SignalHandler::getListenSignalTypes() const {
    return mVecListeningSignalTypes;
}
