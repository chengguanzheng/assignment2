//
// Created by xf-huang on 2020/10/24.
//

#include "Dispatcher.h"
#include "framework/SignalHandler.h"
#include <spdlog/spdlog.h>

#define MODNAME "Dispatcher"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

Dispatcher::Dispatcher()
    : mAbQuitSignalProcess(true)
    , mDisplayHandlerId(-1)
    , mNetworkHandlerId(-1) {}

Dispatcher::~Dispatcher() {
    deactivateHandlers();
}

void Dispatcher::signalProcessLoop() {
    INFO("Starting Dispatcher::signalProcessLoop...");
    Signal signal {};

    while (!mAbQuitSignalProcess) {

        {
            std::unique_lock<std::mutex> lck(mMtxSignalBuffer);
            if (mRBSignalBuffer.empty()) {
                mCvSignalBuffer.wait(lck);
            }
            // process signals according to signal type
            if (!mRBSignalBuffer.empty()) signal = mRBSignalBuffer.pop();

            // signal buffer lock released from here
        }

        if (mAbQuitSignalProcess) break;

        DEBUG(fmt::format("Processing signal {} from {}", explain(signal.signalType),
            explain(signal.originator)));

        {
            std::lock_guard<std::mutex> lck(mMtxHandlerStatus);
            if (!mMapHandlerStatus[signal.originator]) {
                // skip signal if the originator should not be working
                DEBUG(fmt::format("Dropped signal {} from sleeping handler {} of image {}",
                    explain(signal.signalType), explain(signal.originator), signal.imageId));
                continue;
            }

            auto job = Job(signal.imageId, signal.dataSize, signal.originator );
            bool sent = false;
            for (auto handlerId : mMapSignalType2HandlerIds[signal.signalType]) {
                // check working status of the handler type
                if (mMapHandlerStatus[mMapId2Handler[handlerId]->getHandlerType()]) {
                    // Assign jobs to handlers
                    // the job with be stored in handler's job buffer and corresponding condition variable
                    // will be notified to process the job
                    mMapId2Handler[handlerId]->assignJobs(job);
                    DEBUG(fmt::format("Handler : {} was notified by {}",
                        explain(mMapId2Handler[handlerId]->getHandlerType()), explain(signal.signalType)));
                    sent = true;
                }
            }

            if (!sent) {
                if (mMapHandlerStatus[HandlerType::DISPLAY]) {
                    // if descending handlers are not in working mode, send directly to display handler if display id enabled
                    mMapId2Handler[mDisplayHandlerId]->assignJobs(job);
                }

                //                if (mMapHandlerStatus[HandlerType::STATISTIC_REPORT]) {
                //                    mMapId2Handler[mNetworkHandlerId]->assignJobs(job);
                //                }
            }

            // handler working status lock released here
        }
    }
    INFO("Quitting Dispatcher::signalProcessLoop...");
}

bool Dispatcher::registerHandler(const std::shared_ptr<SignalHandler>& handler, bool on) {
    // avoid duplication
    for (auto& p : mMapId2Handler) {
        if (p.second == handler) {
            ERROR(fmt::format("Duplicated registration of type {}", handler->getHandlerType()));
            return false;
        }
    }

    // avoid contradicting working status
    HandlerType handlerType = handler->getHandlerType();
    if (mMapHandlerStatus.count(handlerType) && mMapHandlerStatus[handlerType] != on) {
        INFO(fmt::format("ATTENTION: Working status for {} has been set to {} before!", explain(handlerType),
            mMapHandlerStatus[handlerType]));
    }

    int handlerId = mNextHandlerId;
    ++mNextHandlerId;
    // update id-to-handler mapping
    mMapId2Handler[handlerId] = handler;

    if (handlerType == HandlerType::DISPLAY) {
        mDisplayHandlerId = handlerId;
    }

    //    if (handlerType == HandlerType::STATISTIC_REPORT) {
    //        mNetworkHandlerId = handlerId;
    //    }

    // update signal-type-to-handler-id mapping
    for (auto& listeningSignalType : handler->getListenSignalTypes()) {
        if (listeningSignalType != SignalType::UNDEFINED) {
            mMapSignalType2HandlerIds[listeningSignalType].emplace_back(handlerId);
        }
    }

    // update working status for handler type
    mMapHandlerStatus[handlerType] = on;

    INFO(fmt::format("Module {} send {} and listen to {} registered.",
        explain(handler->getHandlerType()),
        explain(handler->getSendSignalType()),
        explain(handler->getListenSignalTypes())));

    return true;
}

bool Dispatcher::unregisterHandler(const std::shared_ptr<SignalHandler>& handler) {
    auto p = mMapId2Handler.begin();
    while (p != mMapId2Handler.end()) {
        if (p->second == handler) {
            break;
        }
    }

    if (p == mMapId2Handler.end()) {
        ERROR(fmt::format("Trying to unregister an unregistered handler of type {}", handler->getHandlerType()));
        return false;
    }

    if (handler->getHandlerType() == HandlerType::DISPLAY) {
        mDisplayHandlerId = -1;
    }

    auto idx = p->first;
    for (const auto& signalType : handler->getListenSignalTypes()) {
        if (signalType == SignalType::UNDEFINED)
            continue;
        auto& vec = mMapSignalType2HandlerIds[signalType];
        for (auto it = vec.begin(); it != vec.end(); ++it) {
            if (*it == idx) {
                vec.erase(it);
                break;
            }
        }
    }

    return true;
}

void Dispatcher::activateHandlers() {
    // output listening relations
    for (const auto& it : mMapSignalType2HandlerIds) {
        auto signalType = it.first;
        auto handlerIds = it.second;
        std::string handlerTypes;
        for (auto handlerId : handlerIds) {
            handlerTypes += explain(mMapId2Handler[handlerId]->getHandlerType());
            handlerTypes += " ";
        }
        DEBUG(fmt::format("Signal {} -> {}", explain(signalType), handlerTypes));
    }

    // start all handler loops
    for (auto& p : mMapId2Handler) {
        p.second->startDoJobsLoop(); //start all handler thdojobs
    }

    mAbQuitSignalProcess = false;
    mThSignalProcess = std::thread(&Dispatcher::signalProcessLoop, this); //send job to handler's  jobsbuffer
}

void Dispatcher::deactivateHandlers() {
    mAbQuitSignalProcess = true;
    mCvSignalBuffer.notify_all();
    if (mThSignalProcess.joinable()) {
        mThSignalProcess.join();
    }

    for (auto& p : mMapId2Handler) {
        p.second->stopDoJobsLoop();
    }
}

void Dispatcher::emitSignal(Signal& signal) {
    {
        std::lock_guard<std::mutex> lck(mMtxSignalBuffer);
        mRBSignalBuffer.push(signal);
    }
    mCvSignalBuffer.notify_all();
}

void Dispatcher::setWorkingMode(const std::unordered_map<HandlerType, bool>& map) {
    std::lock_guard<std::mutex> lck(mMtxHandlerStatus);
    for (auto it : map) {
        mMapHandlerStatus[it.first] = it.second;
    }
}

const std::unordered_map<HandlerType, bool>& Dispatcher::getWorkingMode() const {
    return mMapHandlerStatus;
}
