//
// Created by xf-huang on 2020/10/24.
//

#pragma once

#include <string>
#include <vector>

// all signal types that the dispatcher handles
enum class SignalType {
    IMAGE_READY,
    DETECT_READY,
    DISPLAY_READY,
    UNDEFINED
};

std::string explain(SignalType signalType);

std::string explain(const std::vector<SignalType> &signalTypes);

// all module types which might register to the dispatcher
enum class HandlerType {
    CAPTURE,
    DETECT,
    DISPLAY,
    UNDEFINED
};

std::string explain(HandlerType moduleType);

// modules send signals to dispatcher
struct Signal {
    Signal() = default;

    Signal(SignalType _signalType, long long id, HandlerType moduleType, int _dataSize)
            : signalType(_signalType), imageId(id), originator(moduleType), dataSize(_dataSize) {}

    SignalType signalType;
    HandlerType originator;
    long long imageId;
    int dataSize;
};

struct Job {
    Job() = default;

    Job(long long id, int dataSize, HandlerType type)
            : imageId(id), originator(type), dataSize(dataSize) {}

    long long imageId;
    int dataSize;
    HandlerType originator;
};
