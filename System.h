#ifndef SYSTEM_H
#define SYSTEM_H

#include <handlers/CaptureHandler.h>
#include <handlers/DisplayHandler.h>
#include "handlers/DetectHandler.h"
#include "framework/Dispatcher.h"

class System {
public:
    System();

    ~System() = default;

    bool start(const std::string &filepath);

    bool stop();

    std::shared_ptr<CaptureHandler> captureHandler;
    std::shared_ptr<DetectHandler> detectHandler;
    std::shared_ptr<DisplayHandler> displayHandler;
    std::shared_ptr<Repository> repository;

private:
    bool started;
    Dispatcher dispatcher;
    std::string filepath;

};

#endif // SYSTEM_H
