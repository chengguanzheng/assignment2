#include "System.h"

#include <spdlog/spdlog.h>

#define MODNAME "System"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

using namespace std;

System::System() : started(false) {
    repository = make_shared<Repository>();
    string key = "img";
    string key2 = "detect";
    captureHandler = make_shared<CaptureHandler>(dispatcher, repository, key);
    detectHandler = make_shared<DetectHandler>(dispatcher, repository, key, key2);
    displayHandler = make_shared<DisplayHandler>(dispatcher, key2);

    dispatcher.registerHandler(captureHandler);
    dispatcher.registerHandler(detectHandler);
    dispatcher.registerHandler(displayHandler);

    dispatcher.activateHandlers();  //每个handler开始执行startDojobsLoop
}

bool System::start(const std::string& filepath_) {
    if (filepath_.empty()) {
        LOG_ERROR("File path is empty!");
        return false;
    }

    filepath = filepath_;
    captureHandler->startCapture(filepath);

    return true;
}

bool System::stop() {
    captureHandler->stopCapture();
    return false;
}
