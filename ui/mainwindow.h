#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "System.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QString filepath;

    std::unique_ptr<System> system;

    std::vector<char> buffer;
};
#endif // MAINWINDOW_H
