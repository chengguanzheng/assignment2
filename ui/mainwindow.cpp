#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>

#include <spdlog/spdlog.h>
#include "opencv2/opencv.hpp"

#define MODNAME "MainWindow"
#define DEBUG(x) spdlog::debug("[{}] {}", MODNAME, x)
#define INFO(x) spdlog::info("[{}] {}", MODNAME, x)
#define LOG_ERROR(x) spdlog::error("[{}] {}", MODNAME, x)

using namespace std;
using namespace cv;

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    system = make_unique<System>();

    connect(ui->select, &QPushButton::clicked, this, [&]() {
        filepath = QFileDialog::getOpenFileName(this, tr("Please select a video file"), QString(), "*.mp4");
        INFO(fmt::format("Selected file {}", filepath.toStdString()));
        ui->file_path->setText(filepath);
    });

    connect(ui->start, &QPushButton::clicked, this, [&]() {
        system->start(filepath.toStdString());
    });

    connect(system->displayHandler.get(), &DisplayHandler::imageReady, this,
            [&](const string &key, long long imageId, int dataSize) {
                if (dataSize > buffer.size()) {
                    buffer.resize(dataSize);
                }
                auto found = system->repository->getById(key, imageId, buffer.data(), dataSize);
                INFO(fmt::format("get here {}", found));
                if (!found) {
                    INFO(fmt::format("Image {} not found in key {}", imageId, key));
                    return;
                }
                auto header = (FrameHeader*)buffer.data();
                auto img = Mat(header->height, header->width, CV_8UC(header->channel), buffer.data() + sizeof(FrameHeader));
                cvtColor(img, img, COLOR_BGR2RGB);
                auto qimg = QImage((uchar*)buffer.data() + sizeof(FrameHeader),
                                   header->width, header->height, header->width * header->channel, QImage::Format_RGB888);
                ui->image->setPixmap(QPixmap::fromImage(qimg).scaled(ui->image->width(), ui->image->height()));
                INFO(fmt::format("get here"));
            });
}

MainWindow::~MainWindow() {
    delete ui;
}

